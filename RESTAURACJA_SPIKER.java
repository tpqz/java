import java.util.*;
class Stol
{
	private int iloscMiejsc;
	
	public Stol()
	{
		this.iloscMiejsc = 4;
	}
}
class Krzeslo
{
	private String kolor;
	
	public Krzeslo(String kolor)
	{
		this.kolor = kolor;
	}
}
class Telewizor
{
	private String program;
	
	public Telewizor(String program)
	{
		this.program = program;
	}	
		
	public void pobierzProgram()
	{
		System.out.println("Aktualnie w Tv leci " + this.program);
	}
}
class Snooker
{
	private Boolean stanStolu;
	
	public Snooker(Boolean stanStolu)
	{
		this.stanStolu = stanStolu;
	}
	
	public void sprawdzStanStolu()
	{
		if (this.stanStolu) System.out.println("Snooker jest wolny");
		else System.out.println("Snooker jest zaj�ty");
	}
}
class Gosc
{
	private String imie;
	public Gosc()
	{
		this.imie = "Go��";
	}
}
class Sala
{
	private Krzeslo[] krzesla;
	private Stol[] stoliki;
	private Telewizor telewizor;
	private Snooker snooker;
	private Gosc[] goscie;
	
	public Sala(int iloscKrzesel, int iloscStolikow, String programTv, int iloscGosci)
	{
		this.krzesla = new Krzeslo[iloscKrzesel];
		this.stoliki = new Stol[iloscStolikow];
		this.telewizor = new Telewizor(programTv);
		this.goscie = new Gosc[iloscGosci];
	}
	public Sala(int iloscKrzesel, int iloscStolikow, String programTv, Boolean stanSnooker, int iloscGosci)
	{
		this.krzesla = new Krzeslo[iloscKrzesel];
		this.stoliki = new Stol[iloscStolikow];
		this.telewizor = new Telewizor(programTv);
		this.snooker = new Snooker(stanSnooker);
		this.goscie = new Gosc[iloscGosci];
	}
	
	public void pobierzIloscGosci()
	{
		System.out.println("W tej sali znajduje si� " + this.goscie.length + " go�ci");
	}
	public void pobierzIloscWolnychStolikow()
	{
		System.out.println("W tej sali jest " + (int)(this.stoliki.length - Math.ceil(((double)this.goscie.length)/4)) + " wolnych stolik�w");
	}
	public void pobierzTv()
	{
		this.telewizor.pobierzProgram();
	}
	public void pobierzStanSnookera()
	{
		if (this.snooker!=null) this.snooker.sprawdzStanStolu();
	}
}
public class RESTAURACJA_SPIKER
{
	private Sala[] sale;
	
	public RESTAURACJA_SPIKER(int iloscSal)
	{
		this.sale = new Sala[iloscSal];
	}
	
	public void wyswietlStanRestauracji()
	{
		for (int i=0; i<this.sale.length; i++)
		{
			System.out.println("Sala " +(i+1)+":");
			this.sale[i].pobierzIloscGosci();
			this.sale[i].pobierzIloscWolnychStolikow();
			this.sale[i].pobierzTv();
			this.sale[i].pobierzStanSnookera();
			System.out.println();
		}
	}
	
    public static void main(String[] args) 
    {
    	Random generator = new Random();
    	String[] stacjeTv = {"Tvn", "Polsat", "Tvp", "Trwam", "Eurosport", "Mtv"};
    	
        RESTAURACJA_SPIKER restauracja = new RESTAURACJA_SPIKER(2);
        int iloscOsobSala1 = generator.nextInt(49);
        int iloscOsobSala2 = generator.nextInt(33);
        String tvSala1 = stacjeTv[generator.nextInt(6)];
        String tvSala2 = stacjeTv[generator.nextInt(6)];
        Boolean snooker;
        if(generator.nextInt(2)==1) snooker = true;
        else snooker = false;
        
        restauracja.sale[0] = new Sala(48,12,tvSala1,iloscOsobSala1);
        restauracja.sale[1] = new Sala(32,8,tvSala2,snooker,iloscOsobSala2);
        
        restauracja.wyswietlStanRestauracji();
        
    }
}