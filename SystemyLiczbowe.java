/**
 * Nazwa: SystemyLiczbowe.java
 * Autor: Mateusz Tapa
 * Grupa: KrDZIs2015
 * Data:  15/11/2015
 * Opis:  Program odczytuje z konsoli liczbę w postaci łańcucha tekstowego oraz wielkość podstawy systemu liczbowego.
 * 		  Wynikiem działania programu jest liczba w systemie dziesiętnym. 
 * 
 * W programie JCreator aby skompilować i uruchomić program należy użyć opcji Build File a następnie Run File.
 * Za pomocą konsoli:
 * Kompilacja: javac SystemyLiczbowe.java
 * Uruchomienie: java SystemyLiczbowe
 */

//zaimportowanie pakietu Scanner
import java.util.Scanner;

public class SystemyLiczbowe{
 
//Stworzenie metody pozwalającej wyswietlić tekst w konsoli
    static void print(String n){
        System.out.println(n);
    }

    public static void main(String[] args){
 
//Zdefiniowanie zmiennej number w postaci łańcucha tekstowego       
        print("Podaj liczbę: ");
        	Scanner numberIn = new Scanner(System.in);
        	String number = numberIn.nextLine();

//Zdefiniowanie zmiennej basis w postaci liczby stałoprzecinkowej
        print("Podaj podstawę systemu liczbowego: ");
        	int basis = numberIn.nextInt();

//Zastosowanie metody valueOf  występującej w klasie  opakowującej Integer oraz wyświetlenie wyniku
		Integer basisNum = Integer.valueOf(number,basis);
        	print("Podana liczba w systemie dziesiętnym: "+ basisNum);
    }


}
