import java.awt.*;
import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

class Container extends JPanel{
	
	JLabel imieText = new JLabel("Imi�");
	JTextField imieField = new JTextField(15);
	
	JLabel nazwiskoText = new JLabel("Nazwisko");
	JTextField nazwiskoField = new JTextField(15);
	
	JLabel wiekText = new JLabel("Rok urodzenia");
	JTextField wiekField = new JTextField(15);
	
	JLabel displayText = new JLabel("Masz: ");
	JTextField displayField = new JTextField(5);
	JLabel displayText2 = new JLabel("lat");
	
	JButton button = new JButton("Oblicz");
	
	
		public Container(){
			
			SpringLayout layout = new SpringLayout();
			setLayout(layout);
			
			add(imieText);
			add(imieField);
			
			layout.putConstraint(SpringLayout.WEST, imieText, 10, SpringLayout.WEST, new JLabel(""));
			layout.putConstraint(SpringLayout.NORTH, imieText, 25, SpringLayout.NORTH,  new JLabel(""));
			
			
			layout.putConstraint(SpringLayout.WEST, imieField, 100, SpringLayout.WEST, new JLabel(""));
			layout.putConstraint(SpringLayout.NORTH, imieField, 25, SpringLayout.NORTH,  new JLabel(""));
			
			add(nazwiskoText);
			add(nazwiskoField);
			
			layout.putConstraint(SpringLayout.WEST, nazwiskoText, 10, SpringLayout.WEST, new JLabel(""));
			layout.putConstraint(SpringLayout.NORTH, nazwiskoText, 65, SpringLayout.NORTH,  new JLabel(""));
			
			layout.putConstraint(SpringLayout.WEST, nazwiskoField, 100, SpringLayout.WEST, new JLabel(""));
			layout.putConstraint(SpringLayout.NORTH, nazwiskoField, 65, SpringLayout.NORTH,  new JLabel(""));
			
			add(wiekText);
			add(wiekField);
			
			layout.putConstraint(SpringLayout.WEST, wiekText, 10, SpringLayout.WEST, new JLabel(""));
			layout.putConstraint(SpringLayout.NORTH, wiekText, 105, SpringLayout.NORTH,  new JLabel(""));
			
			layout.putConstraint(SpringLayout.WEST, wiekField, 100, SpringLayout.WEST, new JLabel(""));
			layout.putConstraint(SpringLayout.NORTH, wiekField, 105, SpringLayout.NORTH,  new JLabel(""));
			
			add(displayText);
			add(displayField);
			add(displayText2);
			
			layout.putConstraint(SpringLayout.WEST, displayText, 10, SpringLayout.WEST, new JLabel(""));
			layout.putConstraint(SpringLayout.NORTH, displayText, 145, SpringLayout.NORTH,  new JLabel(""));
			
			layout.putConstraint(SpringLayout.WEST, displayField, 60, SpringLayout.WEST, new JLabel(""));
			layout.putConstraint(SpringLayout.NORTH, displayField, 145, SpringLayout.NORTH,  new JLabel(""));
			
			layout.putConstraint(SpringLayout.WEST, displayText2, 140, SpringLayout.WEST, new JLabel(""));
			layout.putConstraint(SpringLayout.NORTH, displayText2, 145, SpringLayout.NORTH,  new JLabel(""));
			
			add(button);
			Event hit = new Event();
			button.addActionListener(hit);
			
			layout.putConstraint(SpringLayout.WEST, button, 200, SpringLayout.WEST, new JLabel(""));
			layout.putConstraint(SpringLayout.NORTH, button, 185, SpringLayout.NORTH,  new JLabel(""));
			
		}
		
		private class Event implements ActionListener{
			public void actionPerformed(ActionEvent action){
				
				String input = wiekField.getText();
				int wiek = Integer.parseInt(input);
				int wynik = 2016 - wiek;
				
				String wynikString = Integer.toString(wynik);
				
				displayField.setText(wynikString);
				if(wynik > 100){
					JOptionPane.showMessageDialog(null, "NIE ZYJESZ XD");
				}
				
			}
		}
}

public class GuiKolos {

    public static void main(String[] args) {
        JFrame frame = new JFrame();
       	frame.add(new Container());
       	frame.setTitle("Dane");
        frame.setVisible(true);
        frame.setDefaultCloseOperation(frame.DISPOSE_ON_CLOSE);
        frame.setSize(290,280);
        frame.setResizable(false);
    }
}
