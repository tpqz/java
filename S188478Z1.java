abstract class Odbiornik{
	private String nazwa;
	private int nrProgramu;
	private String[] listaProgram�w = new String[100];

	public Odbiornik(String nazwa){
		this.nazwa = nazwa;

	}

	public void ustawNrProgramu(int nrProgramu){
		System.out.println("Ustawiono program nr: " + nrProgramu);
	}

	abstract public void zmienProgram();
}

class TV extends Odbiornik{

	public TV(String nazwa){
		super(nazwa);
	}

	public void rozjasnij(){
		System.out.println("Telewizor zosta� rozja�niony...");
	}

	public void zmienProgram(){
		System.out.println("Program zosta� zmieniony...");
	}

}

class Radio extends Odbiornik{

	public Radio(String nazwa){
		super(nazwa);
	}

	public void zmienProgram(){
		System.out.println("Program zosta� zmieniony...");
	}

}

public class S188478Z1 {

    public static void main(String[] args) {
        TV telewizor = new TV("Samsung");
        telewizor.zmienProgram();
    }
}
