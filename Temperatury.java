import java.util.Scanner;

public class Temperatury {
        public static void main(String[] args) {
 
 			System.out.println("Podaj temperature w stopniach Celsjusza: ");
 			
 			Scanner celciusIn = new Scanner(System.in);
 				double celcius = celciusIn.nextDouble();
 				
 			double fahrenheit = ((celcius * 9)/5) + 32;
 			double kelvin = celcius + 273.15;
 			
 			System.out.println(celcius + " stopni Celsjusza to " + fahrenheit + " stopni Fahrenheita");
 			System.out.println(celcius + " stopni Celsjusza to " + kelvin + " stopni Kelvina");
 
 
    }
}
