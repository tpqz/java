public class Pojazd {
private String nazwa;
private Silnik silnik;
public Pojazd(String nazwa, String typSilnika) {
this.nazwa = nazwa;
silnik = new Silnik(typSilnika);
}
public void ruszaj(){
System.out.println("Wsiadam do mojego " + nazwa);
silnik.zapal();
System.out.println("Ruszam - gaz do dechy... i w drog�...");
}
public void zatrzymaj(){
System.out.println("Zatrzymuj� si� z piskiem opon...");
silnik.zgas();
System.out.println("Wychodz� z samochodu...");
}
public static void main(String[] args) {
Pojazd pojazd = new Pojazd("Audi Quattro", "Turbo Diesel");
pojazd.ruszaj();
pojazd.zatrzymaj();
}
}
class Silnik {
private String typ;
public Silnik(String typ){
this.typ = typ;
}
public void zapal(){
System.out.println("Zapalam silnik..." + typ);
}
public void zgas(){
System.out.println("Gasz� silnik...");
}
public String pobierzTyp(){
return typ;
}
}