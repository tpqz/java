/**
 * Nazwa: UrzadzeniaMuzyczne.java
 * Autor: Mateusz Tapa
 * Grupa: KrDZIs2015
 * Data:  05/12/2015
 *
 * Opis:  Program przedstawia w j�zyku programowania Java model urz�dzenia odtwarzaj�cego muzyk�,
 * wraz z podstawowymi jego w�a�ciwo�ciami
 *
 * W programie JCreator aby skompilowa� i uruchomi� program nale�y u�y� opcji Build File a nast�pnie Run File.
 * Za pomoc� konsoli:
 * Kompilacja: javac UrzadzeniaMuzyczne.java
 * Uruchomienie: java UrzadzeniaMuzyczne
 */
 
interface wlacza { //utworzenie interfejsu wlacza
    public void wlacz();	
    public void wylacz();
    public boolean czyDziala();
}

interface odtwarza { //utworzenie interfejsu odtwarza
    public void play();
    public void pause();
    public void stop();
}


abstract class Odtwarzacz implements wlacza, odtwarza{ //utworzenie klasy abstrakcyjnej z zaimplementowanymi interfejsami
    protected int glosnosc;
    private boolean dziala;
    protected String marka;
    private int bateria;
    private int pojemnosc;

        public Odtwarzacz(String marka, int bateria, int pojemnosc){ //konstruktor
            this.marka = marka;
            this.dziala = false;
            this.bateria = bateria;
            this.pojemnosc = pojemnosc;
    }

        public void wlacz(){ //metoda odpowiedzialna za wlaczenie urzadzenia
            dziala = true;
        }

        public void wylacz(){ //metoda odpowiedzialna za wylaczenie urzadzenia
            dziala = false;
        }

        public boolean czyDziala(){ //metoda odpowiedzialna za sprawdzenie stanu urzadzenia
            return dziala;
        }

    public abstract void glosniej(); //metoda abstrakcyjna
    public abstract void ciszej(); // metoda abstrakcyjna
}

class OdtwarzaczMP3 extends Odtwarzacz { //utworzenie klasy Odtwarzacz MP3 bedacej podklasa klasy Odtwarzacz
    private int bateria;
    private int pojemnosc;

    public OdtwarzaczMP3(String marka, int bateria, int pojemnosc) { // konstruktor
        super(marka, bateria, pojemnosc);
        glosnosc = 10;
        this.bateria = bateria;
        this.pojemnosc = pojemnosc;
    }


    public void glosniej() { // metoda pozwalajaca zwiekszac glosnosc
        glosnosc++;
    }

    public void ciszej() {	// metoda odpowiedzialna za zciszanie 
        glosnosc--;
    }

    public void play() { // metoda odpowiedzialna uruchomienie odtwarzania
        System.out.println("Odtwarzacz MP3 odtwarza muzyk�");
    }

    public void stop() { // metoda odpowiedzialna za zatrzymanie odtwarzania 
        System.out.println("Odtwarzacz MP3 nie odtwarza muzyki");
    }

    public void pause() { // metoda odpowiedzialna za pauze
        System.out.println("Odtwarzacz MP3 zatrzymany");
    }

    public String toString() { // zwraca warto�� obiekt�w w Stringu
        return "Odtwarzacz MP3 marki " + marka + "\n" +
                "Odtwarzacz jest " +
                ( czyDziala() ? "w��czony" : "wy��czony") + "\n" +
                "G�o�no�� " + glosnosc + "\n" +
                "Pojemno�� " + pojemnosc + "GB " + "\n" +
                "Stan baterii " + bateria + "%";
    }
}
public class UrzadzeniaMuzyczne {

    public static void main (String[] args) {

        OdtwarzaczMP3 odtwarzaczMP3 = new OdtwarzaczMP3("Creative", 78, 16); // utworzenie obiektu OdtwarzaczMP3

        odtwarzaczMP3.wlacz(); // uruchomienie odtwarzacza	
        odtwarzaczMP3.glosniej(); // zwiekszenie glosnosci
        System.out.println(odtwarzaczMP3); 	//wyswietlenie stanu obiektu odtwarzaczMP3	
        odtwarzaczMP3.play(); // uruchomienie odtwarzania 
    }
}