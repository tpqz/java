import java.util.ArrayList;
public class Hall{
     ArrayList<Guest> guests = new ArrayList<Guest>();
     ArrayList<Table> tables = new ArrayList<Table>();
     ArrayList<Televisor> televisors = new ArrayList<Televisor>();
     ArrayList<Snooker> snookers = new ArrayList<Snooker>();
    
     public void addGuest(Guest a){
      guests.add(a);
    }
    
     public void addTable(Table a){
      tables.add(a);
    }
    
     public void addTelevisor(Televisor a){
      televisors.add(a);
    }
  
     public void addSnooker(Snooker a){
      snookers.add(a);
    }
    
}
