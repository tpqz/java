/**
 * @(#)PodatekDochodowy.java
 * @author Mateusz Tapa
 *			KrDZIs2015
 * 			2015/11/06
 * Program dla warto�ci dochodu odczytanej z konsoli wyznaczy warto�� nale�nego podatku.
 * W programie JCreator aby skompilowa� i uruchomi� program nale�y u�y� opcji Build File a nast�pnie Run File.
 * Za pomoc� konsoli:
 * Kompilacja: javac PodatekDochodowy.java
 * Uruchomienie: java PodatekDochodowy
 
*/

//zaimportowanie pakietu Math oraz Scanner
import java.util.Scanner;
import java.lang.Math;

public class PodatekDochodowy {
        public static void main(String[] args) {

	
	//Wy�wietlenie na konsoli podanego w cudzys�owie tesktu	
	System.out.println("Podaj warto�� dochodu w z�ot�wkach: ");
		
//Ta cz�� kodu umo�liwia u�ytkownikowi na zdefiniowanie zmiennej income odpowiedzialnej za warto�� dochodu poprzez wpisanie warto�ci w konsoli.
		Scanner incomeIn = new Scanner(System.in);
			double income = incomeIn.nextDouble();
			
	//Sprawdzenie przez program za pomoc� instrukcji warunkowej if czy warto�� dochodu przekracza kwot� woln� od podatku
	if(income <= 3091){
		
		//Je�eli kwota jest wolna od podatku wy�wietlenie na konsoli tekstu w cudzys�owie
		System.out.println("Kwota wolna od podatku");
	
	//Sprawdzenie czy podana kwota mie�ci si� w ni�szym przedziale skali podatkowej.
	}else if(income > 3091 && income <= 85582){
		
		//Je�eli podana kwota mie�ci si� w podanym przedziale wyliczana zostaje warto�� podatku
		double tax = income*0.18 - 556.02;
			
			//Wynik zostaje zaokr�glony do pe�nej kwoty zgodnie z zasad� oraz wy�wietlony na konsoli
			System.out.println("Podatek wynosi: " + Math.round(tax) + " z�");
			
			
	//Sprawdzenie czy podana kwota mie�ci si� w wy�szym przedziale skali podatkowej.		
	}else if(income > 85582){
			
		//Je�eli podana kwota mie�ci si� w podanym przedziale wyliczana zostaje warto�� podatku
		double surplus = income - 85528;
		double taxHi =  14839.02 + (surplus*0.32);
			
			//Wynik zostaje zaokr�glony do pe�nej kwoty zgodnie z zasad� oraz wy�wietlony na konsoli
			System.out.println("Podatek wynosi: " + Math.round(taxHi) + " z�");
		
		}
	}

}

