/**
 * @(#)BMI.java
 * @author Mateusz Tapa
 *			KrDZIs2015
 * 			2015/10/27
 * Program ma za zadanie wyliczenie BMI na podstawie wpisanych przez u�ytkownika zmiennych.
 * Aby skompilowa� i uruchomi� program nale�y u�y� opcji Build File a nast�pnie Run File w programie JCreator, lub skorzysta� z konsoli polece�.
 */


//zaimportowanie pakietu Math oraz Scanner
import java.lang.Math;
import java.util.Scanner;

public class BMI {

public static double round(double value, int places) {
    if (places < 0) throw new IllegalArgumentException();

    long factor = (long) Math.pow(10, places);
    value = value * factor;
    long tmp = Math.round(value);
    return (double) tmp / factor;
}
        public static void main(String[] args) {

	//Wy�wietlenie na konsoli podanego w cudzys�owie tesktu	
		System.out.println("Podaj mas� cia�a w kilogramach: ");
			
			//Ta cz�� kodu umo�liwia u�ytkownikowi na zdefiniowanie zmiennej bodyMass poprzez wpisanie warto�ci w konsoli
			Scanner mass = new Scanner(System.in);
				double bodyMass	= mass.nextDouble();
				
		//Wy�wietlenie na konsoli podanego w cudzys�owie tesktu	
		System.out.println("Podaj wzrost w centymetrach: ");
			
			//Ta cz�� kodu umo�liwia u�ytkownikowi na zdefiniowanie zmiennej bodyHeight poprzez wpisanie warto�ci w konsoli
			Scanner height = new Scanner(System.in);
				double bodyHeight = height.nextDouble();
				
			//Zamiana jednostek: centymetr�w na metry, oraz zdefiniowanie na potrzeby wzoru BMI zmiennej bodyMeter
			double bodyMeter = bodyHeight/100;
			
			//Zdefiniowanie zmiennej bmi kt�ra jest wynikiem wzoru na BMI wyliczonym z poprzednio podanych przez u�ytkownika zmiennych
			double bmi = bodyMass / (bodyMeter*bodyMeter);
		
		//Wy�wietlenie BMI u�ytkownika
		System.out.println("Twoje BMI jest r�wne: " + round(bmi, 2));
		
		
		//Wy�wietlenie interpretacji wyniku BMI w zale�no�ci od wyniku za pomoc� instrukcji warunkowej if
		if(bmi < 16){
			System.out.println("Jeste� wyg�odzony");
		} else if((bmi > 16) && (bmi <= 16.99)){
			System.out.println("Jeste� wychudzony.");
		} else if((bmi > 16.99) && (bmi <= 18.49)){
			System.out.println("Masz niedowag�");
		} else if((bmi > 18.49) && (bmi <= 24.99)){
			System.out.println("Twoja waga jest prawid�owa");
		} else if((bmi > 24.99) && (bmi <= 29.99)){
			System.out.println("Masz nadwag�");
		} else if((bmi > 29.99) && (bmi <= 34.99)){
			System.out.println("Jeste� na pierwszym stopniu oty�o�ci");
		} else if((bmi > 34.99) && (bmi <= 39.99)){
			System.out.println("Jeste� na drugim stopniu oty�o�ci (oty�o�� kliniczna)");
		} else if(bmi > 39.99) {
			System.out.println("Jeste� na trzecim stopniu oty�o�ci (oty�o�� skrajna)");
		}

		
    }
}
