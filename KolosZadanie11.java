/**
 * Created by Rafal on 2016-01-06.
 */
interface NadajeNrAlbumu {
    void nadajNrAlbumu(int nrAlbumu);
}

class Osoba {
    private String nazwisko;
    private String imie;
    private String pesel;

    public Osoba(String nazwisko, String imie, String pesel) {
        this.nazwisko = nazwisko;
        this.imie = imie;
        this.pesel = pesel;
    }

    @Override
    public String toString() {
        return String.format("\nNazwisko: %s, \nImie: %s, \nPesel: %s", nazwisko, imie, pesel);
    }
}

class Studenciak extends Osoba implements NadajeNrAlbumu {

    private int nrAlbumu;
    private String kierunekStudiow;
    private boolean semestrZaliczony;
    private static int kolejnyNrAlbumu;

    public Studenciak(String nazwisko, String imie, String pesel) {
        super(nazwisko, imie, pesel);
    }

    public void setKierunekStudiow(String kierunekStudiow) {
        this.kierunekStudiow = kierunekStudiow;
    }

    public String getKierunekStudiow() {
        return kierunekStudiow;
    }

    public void zaliczSemestr() {
        semestrZaliczony = true;
        System.out.print("Semestr ustawiono na zaliczony");
    }

    @Override
    public void nadajNrAlbumu(int nrAlbumu) {
        this.nrAlbumu = nrAlbumu;
    }

    @Override
    public String toString() {
        return super.toString() + "\n" + String.format("Numer Albumu: %d, \nKierunek Studiow: %s, \nSemestr zaliczono: %b", nrAlbumu, kierunekStudiow, semestrZaliczony);
    }
}

public class KolosZadanie11 {
    public static void main(String[] args) {

        Studenciak student1 = new Studenciak("Kowalski", "Jan", "95021445375");
        student1.nadajNrAlbumu(188888);
        student1.setKierunekStudiow("Informatyka");
        student1.zaliczSemestr();
        System.out.print(student1.toString());
    }
}