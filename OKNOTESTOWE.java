import javax.swing.*;
import java.awt.*;
public class OKNOTESTOWE extends JFrame {
public OknoTestowe() {
setTitle("Model RGB przestrzeni barw");
setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
setSize(400, 200);
JPanel kontener = new JPanel();
kontener.add(new JButton("R-czerwony"));
kontener.add(new JButton("G-zielony"));
kontener.add(new JButton("B-niebieski"));
add(kontener, BorderLayout.SOUTH);
add(new JTextArea("Paleta barw..."), BorderLayout.CENTER);
setVisible(true);
}
}