import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class ModelRGBPasekStanu extends JFrame {
private JButton przyciskR, przyciskG, przyciskB;
private JPanel panel1, panel2;
private JLabel aktualnyStatus;
public ModelRGBPasekStanu() {
super("Model RGB przestrzeni barw");
setSize(350,300);
setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
setLayout(new BorderLayout());
panel1 = new JPanel();
panel2 = new JPanel();
aktualnyStatus = new JLabel("Aktualny kolor: bia�y");
przyciskR = new JButton("R-czerwony");
przyciskG = new JButton("G-zielony");
przyciskB = new JButton("B-niebieski");
Obsluga zmienNaCzerwony = new Obsluga(Color.red, "czerwony");
Obsluga zmienNaZielony = new Obsluga(Color.green, "zielony");
Obsluga zmienNaNiebieski = new Obsluga(Color.blue, "niebieski");
przyciskR.addActionListener(zmienNaCzerwony);
przyciskG.addActionListener(zmienNaZielony);
przyciskB.addActionListener(zmienNaNiebieski);
panel1.add(przyciskR);
panel1.add(przyciskG);
panel1.add(przyciskB);
panel2.setBackground(Color.white);
add(panel1, BorderLayout.NORTH);
add(panel2, BorderLayout.CENTER);
add(aktualnyStatus, BorderLayout.SOUTH);
}
private class Obsluga implements ActionListener {
private Color kolor;
private String napis;
public Obsluga(Color k, String n) {
this.kolor = k;
this.napis = n;
}
public void actionPerformed(ActionEvent e) {
panel2.setBackground(kolor);
aktualnyStatus.setText("Aktualny kolor: " + napis);
repaint();
}
}
public static void main(String[] args) {
ModelRGBPasekStanu okno = new ModelRGBPasekStanu();
okno.setVisible(true);
}
}