/**
 * @(#)LiczbaPseudolosowa.java
 * @author Mateusz Tapa
 *			KrDZIs2015
 * @version 1.00 2015/10/25
 * Program ma za zadanie wygenerowanie pseudolosowej liczby z przedzia�u <0,1)
 * Aby skompilowa� i uruchomi� program nale�y u�y� opcji Build File a nast�pnie Run File w programie JCreator, lub skorzysta� z konsoli polece�.
 */


//zaimportowanie pakietu Math
import java.lang.Math;
public class LiczbaPseudolosowa {

    public static void main(String[] args) {

			// Wygenerowanie pseudolosowej liczby za pomoc� klasy random 
			System.out.println("Wygenerowana liczba pseudolosowa: " + Math.random());
	
		}
	}

