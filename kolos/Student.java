class Student extends Osoba implements nadajAlbum{
    private int nrAlbumu;
    private String kierunekStudiow;
    private boolean semestrZaliczony;
    private int kolejnyNrAlbumu;

    public Student(String nazwisko, String imie, String pesel){
        super(nazwisko, imie, pesel);

        this.nazwisko = nazwisko;
        this.imie = imie;
        this.pesel = pesel;
    }

    public String pobierzKierunekStudiow(){
        return kierunekStudiow;
    }

    public void ustawKierunekStudiow(String kierunekStudiow){
        this.kierunekStudiow = kierunekStudiow;
    }

    public void zaliczSemestr(){
        this.semestrZaliczony = semestrZaliczony;
        if (semestrZaliczony == true )
            System.out.print("Semestr zaliczony");
        else
            System.out.print("Semestr niezaliczony");
    }

    public void nadajNrAlbumu(){
        this.nrAlbumu = nrAlbumu;
    }

    public String toString(){
        return kierunekStudiow;
    }

}
