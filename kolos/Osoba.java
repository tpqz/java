abstract class Osoba{
    protected String nazwisko;
    protected String imie;
    protected String pesel;

    public Osoba(String nazwisko, String imie, String pesel){
        this.nazwisko = nazwisko;
        this.imie = imie;
        this.pesel = pesel;
    }

    public String toString(){
        return  "Imię: " + imie + "\n" +
                "Nazwisko: " + nazwisko + "\n" +
                "PESEL: " + pesel;
    }

    public abstract void nadajNrAlbumu();
}