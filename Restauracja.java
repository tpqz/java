/**
 * Nazwa: OperacjeNaDatach.java
 * Autor: Mateusz Tapa
 * Grupa: KrDZIs2015
 * Data:  05/12/2015
 *
 * Opis:  Program wy�wietla na konsoli informacje prezentuj�ce stan rzeczywisty restauracji:
 * liczba go�ci w ka�dej sali, liczba wolnych stolik�w, stan zaj�to�ci sto�u do snookera (tylko w sali dla pal�cych), 
 * nazwa programu aktualnie nadawanego w TV.
 *
 * W programie JCreator aby skompilowa� i uruchomi� program nale�y u�y� opcji Build File a nast�pnie Run File.
 * Za pomoc� konsoli:
 * Kompilacja: javac Restauracja.java
 * Uruchomienie: java Restauracja
 */

import java.util.*;



class Hall{ //utworzenie klasy Hall
 
 	//kompozycja
    private Guest[] guests; 
    private Table[] tables;
    private Televisor tv;
    private Snooker snooker;

    public Hall(int guestsQuant, int tableQuant, String tvChannel, boolean snookerTable){ //konstruktor
        this.guests = new Guest[guestsQuant];
        this.tables = new Table[tableQuant];
        this.tv = new Televisor(tvChannel);
        this.snooker = new Snooker(snookerTable);
    }

    public Hall(int guestsQuant, int tableQuant, String tvChannel){
        this.guests = new Guest[guestsQuant];
        this.tables = new Table[tableQuant];
        this.tv = new Televisor(tvChannel);
    }

    public void getGuests() {
        System.out.println("W tej sali aktualnie jest " + this.guests.length + " gosci"); //metoda wyswietlajaca liczbe gosci
    }

    public void getTables(){
        int freeseats = this.tables.length - this.guests.length;
        System.out.println("W tej sali aktualnie jest " + freeseats + " wolnych miejsc"); // metoda wyswietlajaca liczbe wolnych miejsc
    }

    public void getTv(){
        System.out.println("W telewizorze aktualnie nadaje " + this.tv.getChannelOne()); // metoda wyswietlajaca kanal pierwszy
    }

    public void getTvTwo(){
        System.out.println("W telewizorze aktualnie nadaje " + this.tv.getChannelTwo()); // metoda wyswietlajaca kanal drugi
    }

    public void getSnookerTable(){ // metoda wyswietlajaca stan stolu do snookera
        snooker.getSnooker();

    }
}

public class Restauracja { // utworzenie klasy Restauracja
    private Hall[] hallType;


    public Restauracja(int hallQuant) { // konstruktor
        this.hallType = new Hall[hallQuant];
    }

    public void showCondition() { // metoda wyswietlajaca stan sal

            System.out.println("Stan restauracji Europa: "); 
            System.out.println();
            
            System.out.println("Sala dla niepalacych:");
            this.hallType[0].getGuests();
            this.hallType[0].getTables();
            this.hallType[0].getTv();
            System.out.println();

            System.out.println("Sala dla palacych:");
            this.hallType[1].getGuests();
            this.hallType[1].getTables();
            this.hallType[1].getTvTwo();
            this.hallType[1].getSnookerTable();
            System.out.println();
    }
    public static void main(String[] args){

        Restauracja restaurant = new Restauracja(2); //utworzenie obiektu restauracja z dwoma salami
        restaurant.hallType[0] = new Hall(32,48,"Discovery");  // utworzenie sali dla palacych
        restaurant.hallType[1] = new Hall(19,32,"NatGeo", true); // utworzenie sali dla niepalacych

        restaurant.showCondition(); //uzycie metody wyswietlajacej stan restauracji

    }
}