interface Wlaczalny{
	boolean wlacz();
	boolean wylacz();
}

interface GPS{
	boolean wlaczGPS();
	boolean wylaczGPS();
	double[] pobierzWspolrzedne();
}

class Komputer{
	private String procesor;
	
	public Komputer(String procesor){
		this.procesor = procesor;
	}
	
	public void ustawProcesor(String procesor){
		System.out.print("Procesor: " + procesor);
	}
	
	public String pobierzProcesor(){
		return procesor;
	}
	
	public String toString(){
		return "Procesor: " + procesor + "\n";
	}
}

class Tablet extends Komputer implements Wlaczalny, GPS{
	private String nazwa;
	private double waga;
	private int jasnoscEkranu;
	private static int liczbaTabletow;
	
	public Tablet(String nazwa, String procesor, double waga){
		super(procesor);
		this.nazwa=nazwa;
		this.waga=waga;
	}
	
	public Tablet(String nazwa, String procesor){
		this.nazwa=nazwa;
		this.waga=waga;
	}
	
	
}

public class kolosCwiczenia {
    public static void main(String[] args) {
    	
    }
}
