/**
 * Nazwa: KalkulatorGUI.java
 * Autor: Mateusz Tapa
 * Grupa: KrDZIs2015
 * Data:  01/01/2016
 *
 * Opis:  Kalkulator zbudowany z pomoc� graficznego interfejsu u�ytkownika
 *
 * W programie JCreator aby skompilowa� i uruchomi� program nale�y u�y� opcji Build File a nast�pnie Run File.
 * Za pomoc� konsoli:
 * Kompilacja: javac KalkulatorGUI.java
 * Uruchomienie: java KalkulatorGUI
 */


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


class Container extends JPanel{ //klasa odpowiedzialna za kontener
    
    //zadeklarowanie nazw zmiennych
    private JButton one, two, three, four, five, six, seven, eight, nine, zero, add, sub, div, mul, dot, equals; 
    private boolean start; 
    private TextField screen; 
    private double result; 
    private String lastInput;



    public Container(){

        result = 0;
        lastInput = "=";
        start = true;

		//utworzenie wyswietlacza
        setLayout(new BorderLayout());
        screen = new TextField("0");
        add(screen, BorderLayout.NORTH);

		//utworzenie nasluchiwaczy liczb oraz operacji arytmetycznych 
        ActionListener insert = new InsertAction();
        ActionListener operation = new OperationAction();

		//utworzenie panelu z liczbami i operacjami
        JPanel panel = new JPanel();
        GridLayout numbers = new GridLayout(4,4);//utworzenie schematu przyciskow
        panel.setLayout(numbers);

		//utworzenie zmiennych klasy JButton odpowiedzialnych za przyciski
        one = new JButton("1");
        one.addActionListener(insert); //dodanie odpowiedniego nasluchiwacza

        two = new JButton("2");
        two.addActionListener(insert);

        three = new JButton("3");
        three.addActionListener(insert);

        four = new JButton("4");
        four.addActionListener(insert);

        five = new JButton("5");
        five.addActionListener(insert);

        six = new JButton("6");
        six.addActionListener(insert);

        seven = new JButton("7");
        seven.addActionListener(insert);

        eight = new JButton("8");
        eight.addActionListener(insert);

        nine = new JButton("9");
        nine.addActionListener(insert);

        zero = new JButton("0");
        zero.addActionListener(insert);

        add = new JButton("+");
        add.addActionListener(operation);

        sub = new JButton("-");
        sub.addActionListener(operation);

        div = new JButton("/");
        div.addActionListener(operation);

        mul = new JButton("*");
        mul.addActionListener(operation);

        dot = new JButton(".");
        dot.addActionListener(insert);

        equals = new JButton("=");
        equals.addActionListener(operation);

				//dodanie przyciskow do panelu
                panel.add(one);
                panel.add(two);
                panel.add(three);
                panel.add(div);

                panel.add(four);
                panel.add(five);
                panel.add(six);
                panel.add(mul);

                panel.add(seven);
                panel.add(eight);
                panel.add(nine);
                panel.add(add);

                panel.add(zero);
                panel.add(dot);
                panel.add(sub);
                panel.add(equals);

        add(panel, BorderLayout.CENTER);
    }

	//klasa odpowiedzialna za wykonywanie polecen okreslonych przez akcje przyciskow liczb
    private class InsertAction implements ActionListener{
        public void actionPerformed(ActionEvent event) {
            
            String insert = event.getActionCommand();
            if (start){
                start = false;
                screen.setText("");
            }
            screen.setText(screen.getText() + insert);
        }
    }

	//klasa odpowiedzialna za wykonywanie polecen okreslonych przez akcje przyciskow operacji arytmetycznych
    private class OperationAction implements ActionListener{
        public void actionPerformed(ActionEvent event) {
            String operation = event.getActionCommand();
            if (start){
                if(operation.equals("-")) {
                    screen.setText(operation);
                    start = false;
                }
                else lastInput = operation;
            }
            else{
                calculate(Double.parseDouble(screen.getText()));
                lastInput = operation;
                start = true;
            }
        }
    }

	//klasa zajmuj�ca sie obliczaniem operacji arytmetycznych 
    public void calculate(double a){
        if (lastInput.equals("+")) result += a;
        else if (lastInput.equals("-")) result -= a;
        else if (lastInput.equals("/")) result /= a;
        else if (lastInput.equals("*")) result *= a;
        else if (lastInput.equals("=")) result = a;
        screen.setText("" + result);
    }
}

class Window extends JFrame { //klasa odpowiedzialna za wyswietlane okno

    public Window(){
        setTitle("Kalkulator");
        setVisible(true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setSize(350,350);
        setResizable(true);
    }
}

public class KalkulatorGUI { 
    public static void main(String[] args){

        Window window = new Window(); //utworzenie obiektu okna
        Container container = new Container(); //utworzenie obiektu kontenera
        window.add(container); // dodanie kontenera do okna
    }
}
