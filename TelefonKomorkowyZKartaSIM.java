public class TelefonKomorkowyZKartaSIM {
	private KartaSIM sim;
	
	public TelefonKomorkowyZKartaSIM(String numer) {
	sim = new KartaSIM(numer);
}

// definiujemy klas� wewn�trzn� KartaSIM
class KartaSIM {
	private String numerTelefonu;
	
	public KartaSIM(String numer) {
		numerTelefonu = numer;
		}

	public String pobierzNumerTelefonu() {
		return numerTelefonu;
	}
}

	public String toString(){
		return "Telefon z karta SIM o numerze: " + sim.pobierzNumerTelefonu();
	}

public static void main (String[] args) {

TelefonKomorkowyZKartaSIM telefonMarcina = new TelefonKomorkowyZKartaSIM("507864934");
TelefonKomorkowyZKartaSIM telefonKasi = new TelefonKomorkowyZKartaSIM("679342123");

System.out.println (telefonMarcina);
System.out.println (telefonKasi);
}
}
