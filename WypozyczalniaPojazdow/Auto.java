
public class Auto{
   String marka;
   boolean wypozyczone;
    public Auto(String marka, boolean wypozyczone) {
    this.marka = marka;
    this.wypozyczone = wypozyczone;
    }
    
    public Auto(String marka){
     this.marka = marka;
    }

    public void uruchomKlakson(){
        System.out.println("Beep beep");
    }
    
    public String toString(){
        String dostepnosc = wypozyczone ? "wypozyczone" : "dostepne";
        return marka + ", " + dostepnosc;
    }
}