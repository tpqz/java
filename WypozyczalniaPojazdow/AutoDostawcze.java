public class AutoDostawcze extends Auto{
    
    double ladownosc;
    
    public AutoDostawcze(String marka, boolean wypozyczone, double ladownosc){
        super(marka, wypozyczone);
        
        this.ladownosc = ladownosc;
    }
        public String toString(){
        String dostepnosc = wypozyczone ? "wypozyczone" : "dostepne";
        return marka + ", " + dostepnosc + ", " + ladownosc;
    }
}