public class AutoOsobowe extends Auto{
  
    int miejsca;
    
    public AutoOsobowe(String marka, boolean wypozyczone, int miejsca){
        super(marka, wypozyczone);
        
        this.miejsca = miejsca;
    }
    public String toString(){
        String dostepnosc = wypozyczone ? "wypozyczone" : "dostepne";
        return marka + ", " + dostepnosc + ", " + miejsca;
    }
}
