/**
 * Nazwa: SystemyLiczbowe.java
 * Autor: Mateusz Tapa
 * Grupa: KrDZIs2015
 * Data:  06/01/2016
 * Opis:  Zadaniem programu jest konwersja liczby z systemu szesnastkowego na dziesi�tny, z uwzgl�dnieniem obs�ugi b�ed�w.
 * 
 * W programie JCreator aby skompilowa� i uruchomi� program nale�y u�y� opcji Build File a nast�pnie Run File.
 * Za pomoc� konsoli:
 * Kompilacja: javac SystemyLiczbowe.java
 * Uruchomienie: java SystemyLiczbowe
 */
 
import java.util.Scanner;

public class SystemyLiczbowe {

    public static long hex2dec(String hex) { //metoda dokonujaca konwersji z systemu dziesietnego na szesnastkowy
        Long hexNum = Long.valueOf(hex, 16);
        System.out.println(hex + "(16)" + " = " + hexNum + "(10)" + "\n" + "------------------" +
                                                                            "-------------------");
        if (hexNum == 0) { //petla okreslajaca warunek zakonczenia programu
            System.out.println("Program zakonczy� prac�.");
            System.exit(0);
        }else {
            return hexNum;
        }
        return hexNum;
    }

    public static void main(String[] args) {

        String input = null;//zmienna przechowujaca podana szesnastkowa wartosc
        System.out.println("--Konwerter liczb szesnastkowych na system dziesi�tny--");
        System.out.println("Maksymalna warto��: 7fffffffffffffff");
        System.out.println("Aby zako�czy� prac� wpisz liczb� 0" + "\n");
        Scanner in = new Scanner(System.in);

            do { //petla w ktorej sklad wchodzi obsluga bledow
                try {
                    System.out.print("Podaj liczbe w systemie szesnastkowym: ");
                    input = in.nextLine();
                    hex2dec(input); //wywolanie metody odpowiedzialnej za konwersje
                } catch (java.lang.NumberFormatException a) { //gdy wyst�pi b��d wykonywana jest instrukcja poni�ej
                    System.out.println("Wprowadzi�e�/a� nieprawid�ow� liczb�!" + "\n" + "-------------------" +
                                                                                         "------------------");
                }
            } while (input != null); //warunek okreslajacy dzialanie petli
    }
}



