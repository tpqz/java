/**
 * Nazwa: ZbiorFigurGeometrycznych.java
 * Autor: Mateusz Tapa
 * Grupa: KrDZIs2015
 * Data:  06/01/2016
 * Opis:  Zadaniem programu jest wy�wietlenie kwadratu, prostok�ta oraz ko�a w postaci apletu na stronie internetowej.
 * 
 * W programie JCreator aby skompilowa� i uruchomi� program nale�y u�y� opcji Build File a nast�pnie Run File.
 * Za pomoc� konsoli:
 * Kompilacja: javac ZbiorFigurGeometrycznych.java
 * Uruchomienie: java ZbiorFigurGeometrycznych
 */
 
import java.awt.*;
import java.applet.*;
import java.awt.Graphics;

public class ZbiorFigurGeometrycznych extends Applet{
	
	public void init(){ 
    	setBackground(Color.yellow); //ustalenie koloru t�a
    }
	
	public void paint( Graphics g ){
	
		String tekst = "Zbi�r figur geometrycznych";
		Font font = new Font("SansSerif", Font.BOLD, 11); //czcionka napisu
	
		int a = Integer.parseInt(getParameter("bok_a")); //dlugosc parametru okreslona w pliku html
		int b = Integer.parseInt(getParameter("bok_b")); //j.w
		int r = Integer.parseInt(getParameter("promien_r")); //j.w
	
		g.setFont(font); //ustalenie czcionki
		g.drawString(tekst,0,10); //wyswietlenie tekstu
		g.setColor(Color.blue); //kolor obramowania figur
		g.drawRect(65,30,a,a); //polozenie oraz rozmiar figur
		g.drawRect(65,70,a,b);//j.w
		g.drawOval(60,130,r,r);//j.w
	
	}
}
